// Rocket Drone
// Copyright (C) 2022 wavy (wavy@optime.ca)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const room_1 = new Room([
	new Wall(-600, -300, 600, -300),
	new Wall(-600, -300, -600, 300),
	new Wall(600, -300, 600, 300),
	new Wall(-600, 300, 600, 300),
	], {x: -350, y: -285}, {x: 350, y: -285, R: 30})

const room_2 = new Room([
	new Wall(-600, -300, -600, 300),
	new Wall(600, -300, 600, 300),
	new Wall(-600, 300, 600, 300),
	
	new Wall(-50, 0, 50, 0),
	new Wall(-50, 0, -50, -300),
	new Wall(50, 0, 50, -300),
	
	new Wall(-600, -300, -50, -300),
	new Wall(50, -300, 600, -300),
	], {x: -350, y: -285}, {x: 350, y: -285, R: 30})

const room_3 = new Room([
	new Wall(-600, -300, -600, 300),
	new Wall(600, -300, 600, 300),

	new Wall(-600, 300, -50, 300),
	new Wall(50, 300, 600, 300),
	
	new Wall(-50, 0, 50, 0),
	new Wall(-50, 0, -50, -300),
	new Wall(50, 0, 50, -300),

	new Wall(-50, 100, 50, 100),
	new Wall(-50, 100, -50, 300),
	new Wall(50, 100, 50, 300),
	
	new Wall(-600, -300, -50, -300),
	new Wall(50, -300, 600, -300),
	], {x: -350, y: -285}, {x: 350, y: -285, R: 30})

const room_4 = new Room([
	new Wall(-600, -300, -600, 100),
	new Wall(600, -300, 600, 100),
	new Wall(-600, 100, 600, 100),
	
	new Wall(-200, 0, 200, 0),
	new Wall(-200, 0, -200, -300),
	new Wall(200, 0, 200, -300),
	
	new Wall(-600, -300, -200, -300),
	new Wall(200, -300, 600, -300),
	
	], {x: -400, y: -285}, {x: 400, y: -285, R: 30})

const room_5 = new Room([
	new Wall(-450, -300, -450, 200),
	new Wall(450, -300, 450, 200),
	new Wall(-450, -300, 450, -300),
	new Wall(-600, 300, 600, 300),
	
	new Wall(-600, 200, -450, 200),
	new Wall(-600, 200, -600, 300),
	
	new Wall(600, 200, 450, 200),
	new Wall(600, 200, 600, 300),
	
	], {x: -525, y: 215}, {x: 525, y: 215, R: 30})

const room_6 = new Room([
	new Wall(-450, -300, -450, 200),
	new Wall(450, -300, 450, 200),
	new Wall(-450, -300, 450, -300),
	new Wall(-600, 300, -300, 300),
	new Wall(600, 300, 300, 300),
	new Wall(-300, 300, 0, 0),
	new Wall(300, 300, 0, 0),
	
	new Wall(-600, 200, -450, 200),
	new Wall(-600, 200, -600, 300),
	
	new Wall(600, 200, 450, 200),
	new Wall(600, 200, 600, 300),
	
	], {x: -525, y: 215}, {x: 525, y: 215, R: 30})

const room_7 = new Room([
	new Wall(-300, 0, 300, 0),
	new Wall(-300, 0, -300, 1300),
	new Wall(300, 0, 300, 1300),
	new Wall(-300, 1300, 300, 1300),
	
	new Wall(-50, 1200, 50, 1200),
	new Wall(-50, 1200, -50, 1150),
	new Wall(50, 1200, 50, 1150),
	new Wall(-50, 1150, 50, 1150)
	], {x: 0, y: 15}, {x: 0, y: 1215, R: 30})

const room_8 = new Room([
	new Wall(-300, 0, 300, 0),
	new Wall(-300, 0, -300, 600),
	new Wall(-300, 1300, -300, 700),
	new Wall(300, 0, 300, 600),
	new Wall(300, 1300, 300, 700),
	new Wall(-300, 1300, 300, 1300),
	
	new Wall(-300, 700, -35, 700),
	new Wall(300, 700, 35, 700),
	new Wall(-300, 600, -35, 600),
	new Wall(300, 600, 35, 600),
	new Wall(-35, 700, -35, 600),
	new Wall(35, 700, 35, 600),
	
	new Wall(-50, 1200, 50, 1200),
	new Wall(-50, 1200, -50, 1150),
	new Wall(50, 1200, 50, 1150),
	new Wall(-50, 1150, 50, 1150)
	], {x: 0, y: 1215}, {x: 0, y: 15, R: 30})

const room_9 = new Room([
	new Wall(-300, 0, 300, 0),
	new Wall(-300, 0, -300, 600),
	new Wall(-300, 1300, -300, 700),
	new Wall(300, 0, 300, 600),
	new Wall(300, 1300, 300, 700),
	new Wall(-300, 1300, 300, 1300),
	
	new Wall(-300, 700, -35, 700),
	new Wall(300, 700, 35, 700),
	new Wall(-300, 600, -35, 600),
	new Wall(300, 600, 35, 600),
	new Wall(-35, 700, -35, 600),
	new Wall(35, 700, 35, 600),
	
	new Wall(-50, 1200, 50, 1200),
	new Wall(-50, 1200, -50, 1150),
	new Wall(50, 1200, 50, 1150),
	new Wall(-50, 1150, 50, 1150)
	], {x: 0, y: 15}, {x: 0, y: 1215, R: 30})


const tutorial_rooms = [room_1, room_2, room_3, room_4, room_5, room_6, room_7, room_8, room_9]