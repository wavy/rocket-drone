# rocket-drone

A lunar lander type game for the web, made with p5js.

## Setup

Use your preferred method to set up a local http server and host these files, then load index.html to start the game.

## License

See LICENSE for details.

*Note*: the font 'Caron Bulge', a free font by Friendly Fonts, is not covered under the license for the project. See [here](https://friendlyfonts.com/typeface/caron-bulge/) for more information.